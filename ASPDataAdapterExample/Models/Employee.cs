﻿
using System.ComponentModel.DataAnnotations;

namespace ASPDataAdapterExample.Models
{
    public class Employee 
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name ="Salary")]
        public double Salary { get; set; }
        [Required]
        [Display(Name ="Contact")]
        public int Contact { get; set; }

    }
}