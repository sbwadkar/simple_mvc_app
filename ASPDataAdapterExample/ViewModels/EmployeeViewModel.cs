﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ASPDataAdapterExample.Models;

namespace ASPDataAdapterExample.ViewModels
{
    public class EmployeeViewModel
    {

        public EmployeeViewModel()
        {
            Employees = new List<Employee>();
        }
        
        public List<Employee> Employees { get; set; }

    }
}