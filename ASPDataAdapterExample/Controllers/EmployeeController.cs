﻿using System;
using System.Web.Mvc;
using System.Data.SqlClient;
using ASPDataAdapterExample.Models;
using ASPDataAdapterExample.ViewModels;

namespace ASPDataAdapterExample.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddEmployee(Employee employee)
        {
            if (ModelState.IsValid)
            {
                int result = 0;

                string query = "INSERT INTO Emp_details(Emp_Name,Emp_Salary,Emp_Contact) VALUES(@name,@salary,@contact)";

                var dbcon = new DbConnection();

                try
                {
                    SqlCommand cmd = new SqlCommand(query, dbcon.Dbcon);
                    cmd.Parameters.AddWithValue("@name", employee.Name);
                    cmd.Parameters.AddWithValue("@salary", employee.Salary);
                    cmd.Parameters.AddWithValue("@contact", employee.Contact);

                    dbcon.Dbcon.Open();
                    result = cmd.ExecuteNonQuery();
                    if (result > 0)
                        TempData["Message"] = "Record Inserted!";

                    return RedirectToAction("Index");
                }
                catch (Exception e)
                {
                    return Content(e.Message);
                }
                finally
                {
                    dbcon.Dbcon.Close();
                }
            }
            return View();
           
        }

        public ActionResult Index()
        {
            var evm = new EmployeeViewModel();
            
            var dbcon=new DbConnection();
            string query = "SELECT * FROM Emp_details";
            
            try
            {
                SqlCommand cmd = new SqlCommand(query, dbcon.Dbcon);
                dbcon.Dbcon.Open();

                SqlDataReader reader = cmd.ExecuteReader();

                if (!reader.HasRows)
                {
                    return View(new EmployeeViewModel());
                }

                while (reader.Read())
                {
                    Employee emp = new Employee()
                    {
                        Id = Int32.Parse(reader["Emp_id"].ToString()),
                        Name = reader["Emp_Name"].ToString(),
                        Salary = Double.Parse(reader["Emp_Salary"].ToString()),
                        Contact = Int32.Parse(reader["Emp_Contact"].ToString())
                    };

                    evm.Employees.Add(emp);
                }          

             }
            catch
            {
                return HttpNotFound();
            }
            finally
            {
                dbcon.Dbcon.Close();
            }

                return View(evm);
        }

        [HttpGet]
        public ActionResult UpdateEmployee(int? id)
        {
            bool idv = int.TryParse(id.ToString(),out int result);
            if (!idv)
            {
                return HttpNotFound();
            }
            else
            {
                string query = "SELECT * FROM Emp_details WHERE Emp_id=@id";
                DbConnection dbcon = new DbConnection();
                Employee employee = new Employee();
                try
                {
                    SqlCommand cmd = new SqlCommand(query, dbcon.Dbcon);
                    cmd.Parameters.AddWithValue("@id", id);
                    dbcon.Dbcon.Open();

                    SqlDataReader reader = cmd.ExecuteReader();
                    reader.Read();
                    employee.Id = Int32.Parse(reader["Emp_id"].ToString());
                    employee.Name = reader["Emp_Name"].ToString();
                    employee.Salary = Double.Parse(reader["Emp_Salary"].ToString());
                    employee.Contact = int.Parse(reader["Emp_Contact"].ToString());
                }
                catch
                {
                    return HttpNotFound();
                }
                finally
                {
                    dbcon.Dbcon.Close();
                }

                return View(employee);
            }
        }


        [HttpPost]
        public ActionResult UpdateEmployee(Employee employee)
        {
            if (ModelState.IsValid)
            {
                string query = "UPDATE Emp_details SET Emp_Name=@name,Emp_Salary=@salary,Emp_Contact=@contact WHERE Emp_id=@id";

                var dbcon = new DbConnection();

                try
                {
                    SqlCommand cmd = new SqlCommand(query, dbcon.Dbcon);
                    cmd.Parameters.AddWithValue("@name", employee.Name);
                    cmd.Parameters.AddWithValue("@salary", employee.Salary);
                    cmd.Parameters.AddWithValue("@contact", employee.Contact);
                    cmd.Parameters.AddWithValue("@id", employee.Id);

                    dbcon.Dbcon.Open();
                    int result=cmd.ExecuteNonQuery();

                    if (result > 0)
                        TempData["Message"] = "Record Updated!";

                    return RedirectToAction("Index");
                }
                catch
                {
                    return HttpNotFound();
                }
                finally
                {
                    dbcon.Dbcon.Close();
                }
            }
            return View(employee);
        }

        public ActionResult DeleteEmployee(int? id)
        {
            bool idv = int.TryParse(id.ToString(), out int resulta);
            if (!idv)
            {
                return HttpNotFound();
            }
            else
            {

                string query = "DELETE FROM Emp_details WHERE Emp_id=@id";
                DbConnection dbcon = new DbConnection();
                Employee employee = new Employee();
                try
                {
                    SqlCommand cmd = new SqlCommand(query, dbcon.Dbcon);
                    cmd.Parameters.AddWithValue("@id", id);
                    dbcon.Dbcon.Open();

                    int result = cmd.ExecuteNonQuery();
                    if (result > 0)
                        TempData["Message"] = "Record Deleted!";
                }
                catch
                {
                    return HttpNotFound();
                }
                finally
                {
                    dbcon.Dbcon.Close();
                }


                return RedirectToAction("Index");
            }
        }



    }
}