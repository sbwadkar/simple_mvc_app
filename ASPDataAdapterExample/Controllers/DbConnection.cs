﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

namespace ASPDataAdapterExample.Controllers
{
    public class DbConnection
    {
        private SqlConnection con = null;

        //private DbConnection() { }
        
        public SqlConnection Dbcon
        {
            get {

                if (con == null)
                    return con = new SqlConnection(GetConnectionString());
                else
                     return con;
            }
            
        }

        public string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["Connstring"].ConnectionString;
        }
        
    }
}